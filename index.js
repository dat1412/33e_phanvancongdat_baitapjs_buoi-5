// Bài 1: Quản lý sinh viên


function ketQua() {
    var diemDoiTuong = $('select[name=chonDoiTuong] option').filter(':selected').val() * 1;
    var diemKhuVuc = $('select[name=chonKhuVuc] option').filter(':selected').val() * 1;
    var diemMon1 = document.getElementById("diem-mon-1").value * 1;
    var diemMon2 = document.getElementById("diem-mon-2").value * 1;
    var diemMon3 = document.getElementById("diem-mon-3").value * 1;

    var tongDiem = diemMon1 + diemMon2 + diemMon3 + diemDoiTuong + diemKhuVuc;

    var diemChuan = document.getElementById("diem-chuan").value * 1;
    var resultEx1 = document.getElementById("result-ex-1");

    if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
        resultEx1.innerHTML = `Bạn đã rớt. Do có điểm bằng 0`
    }
    if (tongDiem > diemChuan && diemMon1 != 0 && diemMon2 != 0 && diemMon3 != 0) {
        resultEx1.innerHTML = `Bạn đã đậu. Tổng điểm của bạn là: ${tongDiem} `
    }
    if (tongDiem < diemChuan && diemMon1 != 0 && diemMon2 != 0 && diemMon3 != 0) {
        resultEx1.innerHTML = `Bạn đã rớt. Tổng điểm của bạn là: ${tongDiem} `
    }
}

// Bài 2: Tính tiền điện

function tinhTienDien() {
    var fullName = document.getElementById("fullName").value;
    var soKw = document.getElementById("txt-soKw").value * 1;

    var tienDien;

    if ( soKw <= 50) {
        tienDien = soKw * 500;
    } else if (soKw <= 100) {
        tienDien = 50 * 500 + (soKw -50) * 650;
    } else if (soKw <= 200) {
        tienDien = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
    } else if ( soKw <= 350) {
        tienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw -200) * 1100;
    } else {
        tienDien = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
    }

    var resultEx2 = document.getElementById("result-ex-2");
    resultEx2.innerHTML = 
    `<p>Họ tên: ${fullName}</p>
    <p>Tiền điện phải trả là: ${new Intl.NumberFormat().format(tienDien)} VND</p> `
}